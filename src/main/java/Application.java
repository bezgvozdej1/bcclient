import dao.NvsDao;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import utils.FileCodecBase64;

import java.io.IOException;

/**
 * Класс с основными методами приложения
 */
public class Application {

    private String prefix;
    private NvsDao nvsDao = new NvsDao("antkuznetsov", "123456", "127.0.0.1", 6662);

    public Application(String prefix) {
        this.prefix = prefix;
    }

    /**
     * Метод сохраняет файл в блокчейн
     * @param fileName
     * @param filePath
     * @param days
     * @return
     * @throws Exception
     */
    public JSONObject addFile (String fileName, String filePath, int days) throws Exception {

        String file = filePath + "/" + fileName;

        JSONArray params = new JSONArray();

        String value = FileCodecBase64.encode(file);

        params.add(this.prefix + ":" + fileName);
        params.add(value);
        params.add(days);

        return nvsDao.request("name_new", params);
    }

    /**
     * Метод обновляет файл в блокчейне
     * @param fileName
     * @param filePath
     * @param days
     * @return
     * @throws Exception
     */
    public JSONObject updateFile (String fileName, String filePath, int days) throws Exception {

        String file = filePath + "/" + fileName;

        JSONArray params = new JSONArray();

        String value = FileCodecBase64.encode(file);

        params.add(this.prefix + ":" + fileName);
        params.add(value);
        params.add(days);

        return nvsDao.request("name_update", params);
    }

    /**
     * Метод удаляет файла из блокчейна
     * @param fileName
     * @throws IOException
     * @throws ParseException
     */
    public JSONObject deleteFile (String fileName) throws IOException, ParseException {

        JSONArray params = new JSONArray();

        params.add(this.prefix + ":" + fileName);

        return nvsDao.request("name_delete", params);
    }

    /**
     * Метод возвращает файла из блокчейна
     * @param fileName
     * @param targetDir
     * @throws Exception
     */
    public void getFile (String fileName, String targetDir) throws Exception {

        JSONArray params = new JSONArray();

        params.add(this.prefix + ":" + fileName);

        JSONObject jsonObject = nvsDao.request("name_show", params);
        JSONObject result = (JSONObject) jsonObject.get("result");

        String value = result.get("value").toString();

        FileCodecBase64.decode(value, targetDir + "/" + fileName);
    }

    /**
     * Метод возвращает историю изменений файла в блокчейне
     * @param fileName
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public JSONObject getHistory (String fileName) throws IOException, ParseException {

        JSONArray params = new JSONArray();

        params.add(this.prefix + ":" + fileName);

        return nvsDao.request("name_history", params);
    }
}
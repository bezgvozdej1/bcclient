package utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

/**
 * Класс для кодирования и декодирования файлов в Base64
 */
public class FileCodecBase64 {

    /**
     * Метод конвертирует файл в Base64 и возвращает его как строку.
     */
    public static String encode(String sourceFile) throws Exception {
        return Base64.encodeBase64String(loadFileAsBytesArray(sourceFile));
    }

    /**
     * Метод конвертирует строку Base64 в массив байтов и записывает их в файл.
     */
    public static void decode(String source, String targetFile) throws Exception {

        byte[] decodedBytes = Base64.decodeBase64(source);

        writeByteArraysToFile(targetFile, decodedBytes);
    }

    /**
     * Метод загружает файл с диска и преобразует его в массив байтов
     * @param fileName
     * @return
     * @throws Exception
     */
    public static byte[] loadFileAsBytesArray(String fileName) throws Exception {

        File file = new File(fileName);
        int length = (int) file.length();
        BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
        byte[] bytes = new byte[length];
        reader.read(bytes, 0, length);
        reader.close();
        return bytes;
    }

    /**
     * Метод записывает массив байтов в файл
     * @param fileName
     * @param content
     * @throws IOException
     */
    public static void writeByteArraysToFile(String fileName, byte[] content) throws IOException {

        File file = new File(fileName);
        BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
        writer.write(content);
        writer.flush();
        writer.close();
    }
}

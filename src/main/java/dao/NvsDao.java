package dao;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Класс для отправки запросов в сеть Emercoin
 */
public class NvsDao {

    private String login;
    private String password;
    private String server;
    private int port;

    public NvsDao(String login, String password, String server, int port) {
        this.login = login;
        this.password = password;
        this.server = server;
        this.port = port;
    }

    public JSONObject request(String method, JSONArray params) throws IOException, ParseException {

        String url = "http://" + this.login + ":" + this.password + "@" + this.server + ":" + this.port;

        JSONParser parser = new JSONParser();

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);

        post.setHeader("Content-type", "application/json"); // add header

        JSONObject data = new JSONObject();

        data.put("method", method);
        data.put("params", params);

        StringEntity input = new StringEntity(data.toJSONString());

        post.setEntity(input);

        HttpResponse response = client.execute(post);
        System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();

        String line = "";
        Object obj = null;

        while ((line = rd.readLine()) != null) {
            result.append(line);
            obj = parser.parse(line);
        }

        System.out.println(result);

        return (JSONObject) obj;
    }
}
